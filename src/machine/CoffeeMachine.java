package machine;

import java.util.*;

class Coffee {
    String name;
    int water;
    int milk;
    int beans;
    int cost;

    Coffee(String name, int water, int milk, int beans, int cost) {
        this.name = name;
        this.water = water;
        this.milk = milk;
        this.beans = beans;
        this.cost = cost;
    }
}

public class CoffeeMachine {

    private int water;
    private int milk;
    private int beans;
    private int cups;
    private int money;
    private static State state;
    boolean exit;
    boolean input;

    enum State {
        MENU,
        REMAINING,
        BUY,
        FILL,
        FILL_WATER,
        FILL_MILK,
        FILL_COFFEE,
        FILL_CUPS,
        EXIT
    }

    static Coffee espresso = new Coffee("Espresso", 250, 0, 16, 4);
    static Coffee latte = new Coffee("Latte", 350, 75, 20, 7);
    static Coffee cappuccino = new Coffee("Cappuccino", 200, 100, 12, 6);
    static Coffee noCoffee = new Coffee("None", 0,0,0,0);

    CoffeeMachine(int water, int milk, int beans, int cups, int money) {
        this.water = water;
        this.milk = milk;
        this.beans = beans;
        this.cups = cups;
        this.money = money;
        state = State.REMAINING;
        exit = false;
        input = false;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        CoffeeMachine machine = new CoffeeMachine(400, 540, 120, 9, 550);
        do {
            if (machine.input) {
                machine.machineAction(sc.nextLine());
            } else {
                machine.machineAction("");
            }
        } while (!machine.exit);
    }

    public void machineAction(String action) {
        if (input) {
            switch (state) {
                case MENU:
                    userAction(action);
                    break;
                case BUY:
                    chooseDrink(action);
                    state = State.MENU;
                    break;
                case FILL:
                    break;
                case FILL_WATER:
                    water += Integer.parseInt(action);
                    state = State.FILL_MILK;
                    break;
                case FILL_MILK:
                    milk += Integer.parseInt(action);
                    state = State.FILL_COFFEE;
                    break;
                case FILL_COFFEE:
                    beans += Integer.parseInt(action);
                    state = State.FILL_CUPS;
                    break;
                case FILL_CUPS:
                    cups += Integer.parseInt(action);
                    state = State.MENU;
                    break;
            }
            input = false;
        } else {
            switch (state) {
                case MENU:
                    System.out.print("Write action (buy, fill, take, remaining, exit): ");
                    input = true;
                    break;
                case BUY:
                    System.out.print("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino, back - to main menu: ");
                    input = true;
                    break;
                case FILL:
                    state = State.FILL_WATER;
                    break;
                case FILL_WATER:
                    input = true;
                    System.out.print("Write how many ml of water do you want to add: ");
                    break;
                case FILL_MILK:
                    input = true;
                    System.out.print("Write how many ml of milk do you want to add: ");
                    break;
                case FILL_COFFEE:
                    input = true;
                    System.out.print("Write how many grams of coffee beans do you want to add: ");
                    break;
                case FILL_CUPS:
                    input = true;
                    System.out.print("Write how many disposable cups of coffee do you want to add: ");
                    break;
                case REMAINING:
                    input = true;
                    printStatus();
                    input = false;
                    state = State.MENU;
                    break;
                case EXIT:
                    exit = true;
                    break;
                default:
                    break;
            }
        }
    }

    private void chooseDrink(String action) {
        Coffee coffee;
        if (!action.equals("back")) {
            switch (Integer.parseInt(action)) {
                case 1:
                    coffee = espresso;
                    break;
                case 2:
                    coffee = latte;
                    break;
                case 3:
                    coffee = cappuccino;
                    break;
                default:
                    coffee = noCoffee;
                    break;
            }
            if (!coffee.name.equals(noCoffee.name) && checkCoffee(coffee)) {
                System.out.println("I have enough resources, making you a coffee!");
                getCoffee(coffee);
            }
        }
    }
    public void userAction(String action) {
        switch (action) {
            case "buy":
                state = State.BUY;
                break;
            case "fill":
                state = State.FILL;
                break;
            case "take":
                System.out.println(java.lang.String.format("I gave you $%d", money));
                money = 0;
                state = State.MENU;
                break;
            case "remaining":
                state = State.REMAINING;
                break;
            case "exit":
                state = State.EXIT;
                break;
            default:
                break;
        }
    }

    private void getCoffee(Coffee coffee) {
        water -= coffee.water;
        milk -= coffee.milk;
        beans -= coffee.beans;
        money += coffee.cost;
        cups--;
    }

    private void printStatus() {
        System.out.println("The coffee machine has:");
        System.out.println(java.lang.String.format("%d of water", this.water));
        System.out.println(java.lang.String.format("%d of milk", this.milk));
        System.out.println(java.lang.String.format("%d of coffee beans", this.beans));
        System.out.println(java.lang.String.format("%d of disposable cups", this.cups));
        System.out.println(java.lang.String.format("%d of money", this.money));
    }

    private boolean checkCoffee(Coffee coffee) {
        if (water < coffee.water) {
            System.out.println("Sorry, not enough water!");
            return false;
        } else if (milk < coffee.milk) {
            System.out.println("Sorry, not enough milk!");
            return false;
        } else if (beans < coffee.beans) {
            System.out.println("Sorry, not enough coffee beans!");
            return false;
        } else if (cups < 1) {
            System.out.println("Sorry, not enough disposable cups!");
            return false;
        }
        return true;
    }
}

//        System.out.println("Starting to make a coffee");
//        System.out.println("Grinding coffee beans");
//        System.out.println("Boiling water");
//        System.out.println("Mixing boiled water with crushed coffee beans");
//        System.out.println("Pouring coffee into the cup");
//        System.out.println("Pouring some milk into the cup");
//        System.out.println("Coffee is ready!");