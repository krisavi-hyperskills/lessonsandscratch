import java.util.*;

public class RandomGen {
    public static void main(String[] args) {
        //practice4941(args);
        //practice4940(args);
        practice4939(args);
    }

    public static void practice4941(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Random random;
        int k = scanner.nextInt();
        int n = scanner.nextInt();
        double m = scanner.nextDouble();

        long seed = k;
        double gaussian;

        boolean notFound;
        do {
            random = new Random(seed++);
            notFound = false;
            for(int i = 0; i<n; i++) {
                gaussian = random.nextGaussian();
                if (gaussian >= m) {
                    notFound = true;
                    break;
                }
            }

        } while (notFound);
        System.out.println(seed-1);
    }
    public static void practice4940(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int n = scanner.nextInt();
        int k = scanner.nextInt();

        int max = Integer.MAX_VALUE;
        int seed = 0;
        for (int i = a; i <= b; i++) {
            int m = 0;
            Random random = new Random(i);

            System.out.println("Seed: " + i);
            System.out.println();

            System.out.print("num: ");
            for (int j = 0,
                 c; j < n; j++) {
                c = random.nextInt(k);
                System.out.print(c + " ");
                if (m < c) {
                    m = c;
                }
            }
            System.out.println();
            System.out.println("Max: " + m);
            System.out.println();
            System.out.println("Currently saved: " + max);
            System.out.println();

            if (m < max) {
                max = m;
                seed = i;
            }
        }

        System.out.println(max);
        System.out.println(seed);
    }
    public static void practice4939 (String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        Random random = new Random(a + b);
        int sum = 0;
        for (int i = 0; i < n; i++) {
           sum += random.nextInt(b - a + 1) + a;
        }
        System.out.println(sum);
    }
}