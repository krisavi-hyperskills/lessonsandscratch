public class Nan {
    public static void main(String[] args) {
        System.out.println(1 / Double.POSITIVE_INFINITY);
        System.out.println(1 / Double.NaN);
        System.out.println(Double.POSITIVE_INFINITY / 5.0);
        System.out.println(Double.POSITIVE_INFINITY + Double.POSITIVE_INFINITY);
        System.out.println(Double.NaN - Double.NaN);
        Test test = new Test();
        Test test2 = new Test();
    }

    public static int convert (Long val){
        // write your code here
        if (val == null) {
            return 0;
        } else if (val > Integer.MAX_VALUE) {
            return Integer.MAX_VALUE;
        } else if (val < Integer.MIN_VALUE) {
            return Integer.MIN_VALUE;
        }
        return val.intValue();
    }

}


class Test {
    static final int FIELD_1;
    static int field2 = 40; // (1)
    int field3;

    static {
        FIELD_1 = 30; // (2)
        field2 = 50;  // (3)
    }

    static {
        field2 = 80;  // (6)
        Test2();
    }

    {
        Test2();
        field2 = 23;
        field3 = 23;
    }
    public static void Test2() {
        System.out.println("test");
    }

    Test() {
        this.field3 = 2;
        this.field2 = 2;
    }
}