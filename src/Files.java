import java.util.*;
import java.io.*;
public class Files {
    public static void main(String[] args) {
        c4842(args);
        c4841(args);
        c4840(args);
        c4839(args);
        c4838(args);
    }

    public static void c4842(String[] args) {
        File file = new File("src/dataset_91007.txt");
        try (Scanner sc = new Scanner(file)) {

            int greatest = 0;
            String[] numbers = sc.nextLine().split(" ");

            for (String num : numbers) {
                if (Integer.parseInt(num) >= greatest) {
                    greatest = Integer.parseInt(num);
                }
            }
            System.out.println(greatest);
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        }
    }
    public static void c4841(String[] args) {
        File file = new File("src/dataset_91022.txt");
        try (Scanner sc = new Scanner(file)) {

            int count = 0;
            String[] numbers = sc.nextLine().split(" ");

            for (String num : numbers) {
                if (Integer.parseInt(num) >= 9999) {
                    count++;
                }
            }
            System.out.println(count);
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        }
    }
    public static void c4840(String[] args) {
        File file = new File("src/dataset_91033.txt");
        try (Scanner sc = new Scanner(file)) {
            int sum = 0;
            List<String> numbers = new ArrayList<String>();
            while (sc.hasNextLine()) {
                String s = sc.nextLine();
                numbers.add(s);
            }

            for (String num : numbers) {
                sum += Integer.parseInt(num);
            }
            System.out.println(sum);
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        }
    }
    public static void c4839(String[] args) {
        File file = new File("src/dataset_91065.txt");
        try (Scanner sc = new Scanner(file)) {
            int odd = 0;
            int even = 0;
            List<String> numbers = new ArrayList<String>();
            while (sc.hasNextLine()) {
                String s = sc.nextLine();
                if ("0".equals(s)) {
                    break;
                }
                numbers.add(s);
            }

            for (String num : numbers) {
                if (Integer.parseInt(num) % 2 == 0) {
                    even++;
                } else {
                    odd++;
                }
            }
            System.out.println(even);
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        }
    }
    public static void c4838(String[] args) {
        File file = new File("src/dataset_91069.txt");
        try (Scanner sc = new Scanner(file)) {
            List<String[]> numbers = new ArrayList<String[]>();

            sc.nextLine();
            String temp = sc.nextLine();
            String year = temp.split("\t")[0];
            long previous = Long.parseLong(temp.split("\t")[1].replace(",",""));
            long increase = 0;

            while (sc.hasNextLine()) {
                String s = sc.nextLine();
                numbers.add(s.split("\t"));
            }

            for (String[] num : numbers) {
                if (increase < Long.parseLong(num[1].replace(",",""))-previous) {
                    increase = Long.parseLong(num[1].replace(",",""))-previous;
                    year = num[0];
                }
                previous = Long.parseLong(num[1].replace(",",""));
            }
            System.out.println(year);
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        }
    }
}
