import java.util.*;

class multidim {
    public static void main(String[] args) {
        // put your code here
        Scanner sc = new Scanner(System.in);

        String result = "YES";

        String[][] lst = new String[4][4];

        for (int i = 0; i < 4; i++) {
            String line = sc.nextLine();
            for (int j = 0; j < 4; j++) {
                lst[i][j] = String.valueOf(line.charAt(j));
            }
        }

        for (int i = 1; i < 4; i++) {
            for (int j = 1; j < 4; j++) {
                if (lst[i-1][j-1].equals(lst[i-1][j]) && lst[i-1][j].equals(lst[i][j-1]) && lst[i][j-1].equals(lst[i][j])) {
                    result = "NO";
                    break;
                }
            }
        }
        System.out.println(result);
    }
}
