import static java.util.Arrays.*;

public class AsciiCharSequence implements CharSequence {
    private byte[] sequence;
    // implementation
    private AsciiCharSequence(byte[] sequence) {
        this.sequence = sequence;
    }

    public int length() {
        return sequence.length;
    }

    public char charAt(int idx) {
        return (char) sequence[idx];
    }

    public CharSequence subSequence(int start, int end) {
        //byte[] cs = new byte[end-start];
        //System.arraycopy(sequence, start, cs,0, end-start);
        //return new AsciiCharSequence(cs);
        return new AsciiCharSequence(copyOfRange(sequence, start, end));
    }

    public String toString() {
        return new String(sequence);
    }
}
