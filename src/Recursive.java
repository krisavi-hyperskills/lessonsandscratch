import java.util.Scanner;

public class Recursive {
    public static void main(String[] args) {
        c3123(args);
    }


    /* Fix this method */
    public static void printReverseCharByChar(String s) {
        if (s.length() > 0) {
            int last = s.length() - 1;
            printReverseCharByChar(s.substring(s.length()-last, s.length()));
            System.out.print(s.charAt(0));
            /*

            System.out.print(s.charAt(last));
            printReverseCharByChar(s.substring(0, last));
             */
        }
    }

    /* Do not change code below */
    public static void c3119(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        printReverseCharByChar(scanner.nextLine());
    }






    public static double pow(double a, long n) {
        // write your code here
        if (n == 0) {
            return 1.0;
        }
        if (n%2 == 0) {
            return pow(a*a, n/2);
        } else {
            return a*pow(a, n-1);
        }
    }

    /* Do not change code below */
    public static void c3118(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final double a = Double.parseDouble(scanner.nextLine());
        final int n = Integer.parseInt(scanner.nextLine());
        System.out.println(pow(a, n));
    }

    public static long fib(long n){
        // write your code here
        if (n == 2) {
            return -1;
        }
        if (n <= 1) {
            return n;
        }
        return fib(n - 2) - fib(n - 1);
    }

    /* Do not change code below */
    public static void c3123(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(fib(n));
    }
}
