import java.util.*;

public class Sets {
    public static void main(String[] args) {
        a();
        b();
        c();
        d();
        e();


        Map<String, Integer> map = new TreeMap<>();

        map.put("Gamma",  3);
        map.put("Omega", 24);
        map.put("Alpha",  1);

        //write your code here
        for (Map.Entry item : map.entrySet()){

        }
    }

    private static void a() {
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);

        Set<Integer> set2 = new HashSet<>();
        set2.add(1);
        set2.add(2);
        set2.add(3);

        System.out.println(set1.equals(set2));
    }
    private static void b() {
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);

        Set<Integer> set2 = new HashSet<>();
        set2.add(3);
        set2.add(2);
        set2.add(1);

        System.out.println(set1.equals(set2));
    }
    private static void c() {
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);

        Set<Integer> set2 = new LinkedHashSet<>();
        set2.add(1);
        set2.add(3);
        set2.add(2);

        System.out.println(set1.equals(set2));
    }
    private static void d() {
        Set<Integer> set1 = new LinkedHashSet<>();
        set1.add(1);
        set1.add(3);
        set1.add(2);

        Set<Integer> set2 = new LinkedHashSet<>();
        set2.add(1);
        set2.add(3);

        System.out.println(set1.equals(set2));
    }
    private static void e() {
        SortedSet<Integer> set1 = new TreeSet<>();
        set1.add(1);
        set1.add(3);

        Set<Integer> set2 = new LinkedHashSet<>();
        set2.add(3);
        set2.add(1);

        System.out.println(set1.equals(set2));
    }

    public static TreeSet<Integer> unionTreeLargeNumber(Set<Integer> set1, Set<Integer> set2, Set<Integer> set3) {
        TreeSet<Integer> maximums = new TreeSet<Integer>();
        maximums.add(Collections.max(set1));
        maximums.add(Collections.max(set2));
        maximums.add(Collections.max(set3));
        return maximums;
    }

    public static NavigableMap<Integer, String> getSubMap(NavigableMap<Integer, String> map){
        if (map.firstKey() % 2 != 0) {
            return map.subMap(map.firstKey(), true, map.firstKey()+4, true).descendingMap();
        } else {
            return map.subMap(map.lastKey()-4, true, map.lastKey(), true).descendingMap();
        }
    }

}
