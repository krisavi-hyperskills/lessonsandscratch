public class staticTest {

    public static void main(String args[]) {

        Person person = new Person("R. Johnson");

        System.out.println(person.getNextId()); // (1)
        new Programmer(1);

        Cat cat1 = new Cat("CAT1", 11);
        System.out.println(Cat.getNumberOfCats());
        Cat cat2 = new Cat("CAT2", 11);
        System.out.println(Cat.getNumberOfCats());
        Cat cat3 = new Cat("CAT3", 11);
        System.out.println(Cat.getNumberOfCats());
        Cat cat4 = new Cat("CAT4", 11);
        System.out.println(Cat.getNumberOfCats());
        Cat cat5 = new Cat("CAT5", 11);
        System.out.println(Cat.getNumberOfCats());
        Cat cat6 = new Cat("CAT6", 11);
        System.out.println(Cat.getNumberOfCats());
        Cat cat7 = new Cat("CAT7", 11);
        System.out.println(Cat.getNumberOfCats());
        Cat cat8 = new Cat("CAT8", 11);
        System.out.println(Cat.getNumberOfCats());
    }

    public static class TeamLead {
        private int numTeamLead;

        public TeamLead(int numTeamLead) {
            this.numTeamLead = numTeamLead;
            employ();
        }

        protected void employ() {
            System.out.println(numTeamLead + " teamlead");
        }

    }

    public static class Programmer extends TeamLead {
        private int numProgrammer;

        public Programmer(int numProgrammer) {
            super(numProgrammer);
            this.numProgrammer = numProgrammer;
            employ();

        }

        protected void employ() {
            System.out.println(numProgrammer + " programmer");
        }
    }
}

class Person {

    private static long nextId = 1;

    long id;
    String name;

    public Person(String name) {
        this.name = name;
        this.id = nextId;
        this.nextId++; // (2)
    }

    public static long getNextId() { // (3)
        return nextId;
    }
}

class Cat {
    static int counter = 0;

    String name;
    int age;
    // write static and instance variables

    Cat(String name, int age) {
        this.name = name;
        this.age = age;
        counter += 1;
        /*
        if (counter > 5) {
            System.out.println("You have too many cats");
        }*/
        // implement the constructor
        // do not forgot to check the number of cats
    }

    static int getNumberOfCats() {
        // implement the static method
        for (int i = counter; i > 5; i--) {
            System.out.println("You have too many cats");
        }
        return counter;
    }
}
