import java.util.*;

public class arraylists {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        ArrayList<String> list = new ArrayList<>(Arrays.asList(str.split(" ")));
        int num = Integer.parseInt(sc.nextLine());
        ArrayList<Integer> numList = new ArrayList<>();
        boolean found = false;
        int i = 0;
        while (!found) {
            for (String s : list) {
                if (Integer.parseInt(s) == num + i || Integer.parseInt(s) == num - i) {
                    numList.add(Integer.parseInt(s));
                    found = true;
                }
            }
            i++;
        }
        for (int j : numList) {
            System.out.print(j + " ");
        }
    }
    public static ArrayList<Integer> concatPositiveNumbers(ArrayList<Integer> l1, ArrayList<Integer> l2) {
        for (Integer i : l1) {
            if (i > 0) {
                l1.remove(i);
            }
        }
        for (Integer i : l2) {
            if (i > 0) {
                l1.add(i);
            }
        }
        return l1; // write your code here
    }
}
