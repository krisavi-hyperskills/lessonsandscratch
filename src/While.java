import java.util.*;

public class While {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double m = scanner.nextDouble();
        double p = scanner.nextDouble();
        double k = scanner.nextDouble();
        int c = 0;
        while (m < k) {
            m *= 1.0 + p / 100.0;
            c++;
        }
        System.out.println(c);
    }
}
