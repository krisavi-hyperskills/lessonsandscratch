import java.util.*;

class iterate {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ArrayList<String> list = new ArrayList<>(Arrays.asList(sc.nextLine().split(" ")));

        System.out.println(inAlphabethicalOrder(list));
    }

    private static boolean inAlphabethicalOrder(ArrayList<String> list) {
        String lastString = null;
        for (String s : list) {
            if (lastString == null || lastString.isEmpty()){
                lastString = s;
                continue;
            }

            if (charAlphabethicalOrder(s.toCharArray())) {
                if (lastString.length() < s.length()) {
                    continue;
                } else if (lastString.length() == s.length()){
                    if (lastString.equals(s) || cmpCharInStrings(s.toCharArray(), lastString.toCharArray())) {
                        lastString = s;
                        continue;
                    }
                }
            }

            return false;
        }

        return true;
    }

    private static boolean cmpCharInStrings(char[] currentString, char[] lastString) {
        for (int i = 0; i < currentString.length; i++) {
            if (lastString[i] > currentString[i]) {
                return false;
            }
        }
        return true;
    }


    private static boolean charAlphabethicalOrder(char[] string) {
        char last = 0;

        for (char c : string) {
            if (last <= c){
                last = c;
                continue;
            }
            return false;
        }
        return true;
    }


    public static void maindemo(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] arr = scanner.nextLine().trim().split(" ");

        boolean alphabetical = true;
        String lastOne = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i].compareTo(lastOne) < 0) {
                alphabetical = false;
                break;
            }
            lastOne = arr[i];
        }

        System.out.println(alphabetical);
    }
}