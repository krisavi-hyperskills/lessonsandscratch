import java.util.*;

public class tictactoe {
    public static void main(String[] args) {
        String[][][] boards = new String[][][]{
                {
                        {"X", "X", "X"},
                        {"O", "O", ""},
                        {"", "O", ""}
                }, {
                {"X", "O", "X"},
                {"O", "X", "O"},
                {"X", "X", "O"}
        }, {
                {"X", "O", "O"},
                {"O", "X", "O"},
                {"X", "X", "O"}
        }, {
                {"X", "O", "X"},
                {"O", "O", "X"},
                {"X", "X", "O"}
        }, {
                {"X", "O", ""},
                {"O", "O", "X"},
                {"", "X", ""}
        }
        };
        /*
        Scanner scanner = new Scanner(System.in);
        String[][] board = new String[3][3];
        System.out.println(scanner.next());
        for (int i = 0; i<board.length; i++) {
            for (int j = 0; j<board[i].length; j++) {
                board[i][j] = "X";
            }
        }
        */

        for (String[][] board : boards) {
            for (String[] row:  board){
                for (String cell : row) {
                    if ("".equals(cell)) {
                        System.out.print(" ");
                    }
                    System.out.print(cell + " ");
                }
                System.out.println();
            }

            boolean isDraw = true;
            if (board[0][0].equals(board[1][1]) && board[1][1].equals(board[2][2])) {
                System.out.println(board[0][0] + " wins");
            } else if (board[2][0].equals(board[1][1]) && board[1][1].equals(board[0][2])) {
                System.out.println(board[0][2] + " wins");
            } else {
                for (int i = 0; i < board.length; i++) {
                    if (board[i][0].equals(board[i][1]) && board[i][1].equals(board[i][2])) {
                        System.out.println(board[i][0] + " wins");
                        isDraw = false;
                        break;
                    } else if (board[0][i].equals(board[1][i]) && board[1][i].equals(board[2][i])) {
                        System.out.println(board[0][i] + " wins");
                        isDraw = false;
                        break;
                    }

                    for (int j = 0; isDraw && (j < board[i].length); j++) {
                        if (board[i][j].isEmpty()) {
                            System.out.println("Game not finished");
                            isDraw = false;
                            break;
                        }
                    }
                }
                if (isDraw) {
                    System.out.println("Draw");
                }

            }
        }
    }
}
